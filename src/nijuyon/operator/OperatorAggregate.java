package nijuyon.operator;

import java.util.ArrayList;
import java.util.List;

/**
 * An aggregate of several mathematical operators.
 */
public class OperatorAggregate {
    private List<Operator> operators;
    private int[] numbers;
    private static final int DESIRED_RESULT = 24;
    private static final double DELTA = 0.000001;

    /**
     * Constructor.
     */
    public OperatorAggregate(Operator a, Operator b, Operator c) {
        operators = new ArrayList<>();
        operators.add(a);
        operators.add(b);
        operators.add(c);
    }

    /**
     * Apply all of the operators in succession to the array of numbers.
     */
    public boolean operate(int[] numbers) {
        this.numbers = numbers;

        double result = operators.get(2).operate(
                operators.get(1).operate(
                        operators.get(0).operate(numbers[0], numbers[1]), numbers[2]), numbers[3]);

        return checkResult(result);
    }

    /**
     * Check if the result is close enough to the desired result.
     */
    private boolean checkResult(double result) {
        result = result - DESIRED_RESULT;
        return -DELTA < result && result < DELTA;
    }

    /**
     * Get a string with the mathematical expression.
     */
    public String toString() {
        return "((" + numbers[0] + " " + operators.get(0).toString() + " " + numbers[1] + ")"
                + " " + operators.get(1).toString() + " " + numbers[2] + ")"
                + " " + operators.get(2).toString() + " " + numbers[3];
    }
}
