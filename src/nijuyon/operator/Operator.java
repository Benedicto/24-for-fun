package nijuyon.operator;

/**
 * Interface for mathematical operators.
 */
public interface Operator {
    double operate(double a, double b);
    String toString();
}
