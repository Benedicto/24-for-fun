import java.util.Arrays;

/**
 * Created by zhenhua.xu on 12/28/16.
 */
public class Ben24Solver implements A24Solver {

    public boolean solve(int[] numbers) {
        return permuteNumbers(numbers, 0);
    }

    private boolean permuteNumbers(int[] numbers, int start) {
        if (start >= numbers.length) {
            return permuteOperators(numbers);
        } else {
            boolean foundSolution = false;
            for (int j = start; j < numbers.length; j++) {
                swap(numbers, start, j);
                foundSolution = permuteNumbers(numbers, start + 1) || foundSolution;
                swap(numbers, start, j);
            }
            return foundSolution;
        }
    }

    private void swap(int[] a, int i, int j) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    private boolean permuteOperators(int[] numbers){
        char[] operators = new char[numbers.length - 1];
        return permuteOperators(numbers, numbers[0], 1, operators);
    }

    private boolean permuteOperators(int[] numbers, double current, int next, char[] operators) {
        if(next == numbers.length) {
            if(Math.abs(24 - current) < 1e-10) {
                printSolution(numbers, operators);
                return true;
            } else {
                return false;
            }
        } else {
            boolean solutionFound = false;
            operators[next-1] = '+';
            solutionFound = permuteOperators(numbers, current + numbers[next], next + 1, operators);
            operators[next-1] = '-';
            solutionFound = permuteOperators(numbers, current - numbers[next], next + 1, operators) || solutionFound;
            operators[next-1] = '*';
            solutionFound = permuteOperators(numbers, current * numbers[next], next + 1, operators) || solutionFound;
            operators[next-1] = '/';
            solutionFound = permuteOperators(numbers, current / numbers[next], next + 1, operators) || solutionFound;
            return solutionFound;
        }
    }

    private void printSolution(int[] numbers, char[] operators) {
        StringBuilder solution = new StringBuilder();
        for (int i=0; i<operators.length; i++) {
            solution.append('(');
        }
        solution.append(numbers[0]);
        for (int i=0; i<operators.length; i++) {
            solution.append(operators[i]).append(numbers[i+1]).append(')');
        }
        System.out.println(solution);
    }

    public static int[] generate4Numbers() {
        int[] temp = new int[4];
        for (int i=0; i<4; i++) {
            temp[i] = (int) (1 + Math.random() * 9);
        }
        return temp;
    }

    public static void main(String[] args) {
        Ben24Solver solver = new Ben24Solver();
        int[] numbers = new int[] {2,3,5,8};
        System.out.println("4 random numbers: " + Arrays.toString(numbers));
        if (!solver.solve(numbers)) {
            System.out.print("There is no solution!");
        }
    }
}
