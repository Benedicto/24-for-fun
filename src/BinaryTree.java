/**
 * Created by XUZH0001 on 12/27/2016.
 */
public class BinaryTree {

    Node root;

    public BinaryTree(String[] numbers) {
        root = new Node();
        Node currentNode = root;
        for (int i=3; i>0; i--) {
            if (i==1) {
                currentNode.left = new Node(numbers[0]);
                currentNode.right = new Node(numbers[i]);
                break;
            }
            currentNode.left = new Node();
            currentNode.right = new Node(numbers[i]);
            currentNode = currentNode.left;
        }
    }

    private class Node {
        private String symbol;
        Node left;
        Node right;

        public Node() {
            symbol = null;
            left = null;
            right = null;
        }

        public Node(String symbol) {
            this.symbol = symbol;
            left = null;
            right = null;
        }

    }

    public double traverseInOrder(Node node) {
        if (node.left!=null) {
            if (node.symbol == "+") {
                return traverseInOrder(node.left) + Integer.parseInt(node.right.symbol);
            } else if (node.symbol == "-") {
                return traverseInOrder(node.left) - Integer.parseInt(node.right.symbol);
            } else if (node.symbol == "*") {
                return traverseInOrder(node.left) * Integer.parseInt(node.right.symbol);
            } else {
                return (double) (traverseInOrder(node.left) / Integer.parseInt(node.right.symbol));
            }
        } else {
            return Integer.parseInt(node.symbol);
        }
    }

    public void printTree(Node node) {

        if (node.left!=null) {
            System.out.print("(");
            printTree(node.left);
        }
        System.out.print(node.symbol + " ");
        if (node.right!=null) {
            System.out.print(node.right.symbol);
            System.out.print(") ");
        }

    }

    public void setOperators(String[] operators) {
        root.symbol = operators[0];
        root.left.symbol = operators[1];
        root.left.left.symbol = operators[2];
    }

}