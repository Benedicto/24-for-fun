import nijuyon.operator.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Finds all ways to add, subtract, multiply, or divide four input numbers to get a result of 24.
 */
public class Jonas24Solver implements A24Solver {

    List<OperatorAggregate> operatorAggregates = generateOperatorAggregates();

    @Override
    public boolean solve(int[] numbers) {
        List<int[]> numberCombinations = generateNumberCombinations(numbers);
        List<String> solutions = findSolution(numberCombinations, operatorAggregates);
        printSolutions(solutions);
        return !solutions.isEmpty();
    }

    /**
     * Get the numbers as input from the user.
     */
    private static int[] numberInput() throws Exception {
        System.out.println("Jonas24Solver");
        System.out.println("Finds all ways to add, subtract, multiply, or divide four numbers to get a result of 24");
        System.out.println("Four numbers are needed");
        int[] numbers = new int[4];
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        for (int i = 0; i < 4; i++) {
            System.out.println("Please input the next number");
            int number;
            String rad = br.readLine();
            number = Integer.parseInt(rad);
            numbers[i] = number;
        }

        System.out.println("Numbers: " + numbers[0] + " " + numbers[1] + " " + numbers[2] + " " + numbers[3] + " ");
        return numbers;
    }

    /**
     * Generate a list of all unique combinations of numbers.
     */
    public List<int[]> generateNumberCombinations(int[] numbers) {
        List<int[]> numberCombinations = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (j == i) {
                    continue;
                }
                for (int k = 0; k < 4; k++) {
                    if (k == i || k == j) {
                        continue;
                    }
                    for (int l = 0; l < 4; l++) {
                        if (l == i || l == j || l == k) {
                            continue;
                        }

                        int[] combination = new int[4];
                        combination[0] = numbers[i];
                        combination[1] = numbers[j];
                        combination[2] = numbers[k];
                        combination[3] = numbers[l];
                        numberCombinations.add(combination);
                    }
                }
            }
        }

        return numberCombinations;
    }

    /**
     * Generate a list of all possible combinations of operators.
     */
    public List<OperatorAggregate> generateOperatorAggregates() {
        Operator[] operators = {new Plus(), new Minus(), new Multiply(), new Divide()};
        List<OperatorAggregate> operatorAggregates = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                for (int k = 0; k < 4; k++) {
                    operatorAggregates.add(new OperatorAggregate(operators[i], operators[j], operators[k]));
                }
            }
        }

        return operatorAggregates;
    }

    /**
     * Find all possible solutions.
     */
    public List<String> findSolution(List<int[]> numberCombinations, List<OperatorAggregate> operatorAggregates) {
        List<String> solutions = new ArrayList<>();

        for (int[] numberCombination : numberCombinations) {
            for (OperatorAggregate aggregate : operatorAggregates) {
                if (aggregate.operate(numberCombination)) {
                    solutions.add(aggregate.toString());
                }
            }
        }

        // Remove duplicates
        solutions = solutions.stream().distinct().collect(Collectors.toList());

        return solutions;
    }

    /**
     * Print all of the found solutions to the screen.
     */
    private void printSolutions(List<String> solutions) {
        if (0 == solutions.size()) {
            System.out.println("No solution found");
        } else {
            System.out.println(solutions.size() + " solutions found:");
        }

        for (String solution : solutions) {
            System.out.println(solution);
        }
    }
}
