import java.util.ArrayList;

/**
 * Created by XUZH0001 on 12/27/2016.
 */


public class Beatrix24Solver implements A24Solver{
    BinaryTree tree;
    private ArrayList<String[]> operators = new ArrayList<>();
    private ArrayList<int[]> permutatedNumbers = new ArrayList<>();

    public Beatrix24Solver() {
        this.generateOperators();
    }

    public int[] generate4Numbers() {
        int[] temp = new int[4];
        for (int i=0; i<4; i++) {
            temp[i] = (int) (1 + Math.random() * 9);
        }
        return temp;
    }

    public boolean solve(int[] numbers) {
        this.permutatedNumbers.clear();
        this.permute(numbers, 0, 3);

        for (int i=0; i<permutatedNumbers.size(); i++) {
            constrcutTree(permutatedNumbers.get(i));
            for (int j=0; j<operators.size(); j++) {
                tree.setOperators(operators.get(j));
                if (Math.abs(tree.traverseInOrder(tree.root)-24)<1e-10) {
                    tree.printTree(tree.root);
                    return true;
                }
            }
        }
        return false;
    }

    public void constrcutTree(int[] numbers) {
        String[] temp = new String[4];
        for (int i=0; i<4; i++) {
            temp[i] = Integer.toString(numbers[i]);
        }
        tree = new BinaryTree(temp);
    }

    private void permute(int[] numbers, int start, int end) {
        if (start==end) {
            int[] temp = new int[4];
            for (int i=0; i<4; i++) {
                temp[i] = numbers[i];
            }
            permutatedNumbers.add(temp);
            return;
        }
        for (int i=start; i<=end; i++) {
            swap(numbers, start, i);
            permute(numbers, start+1, end);
            swap(numbers, start, i);
        }
    }

    private void swap(int[] array, int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    private void generateOperators() {
        String[] symbols = {"+", "-", "*", "/"};
        String[] temp = new String[3];
        for (int i=0; i<4; i++) {
            temp[0] = symbols[i];
            for (int j=0; j<4; j++) {
                temp[1] = symbols[j];
                for (int k=0; k<4; k++) {
                    temp[2] = symbols[k];
                    String[] newOperatorString = new String[3];
                    for (int l=0; l<3; l++) {
                        newOperatorString[l] = temp[l];
                    }
                    operators.add(newOperatorString);
                }
            }
        }
    }

    private void printPermutations() {
        for (int i=0; i<permutatedNumbers.size(); i++) {
            for (int j=0; j<4; j++) {
                System.out.print(permutatedNumbers.get(i)[j] + " ");
            }
            System.out.println();
        }
    }

    private void printOperators() {
        for (int i=0; i<operators.size(); i++) {
            for (int j=0; j<3; j++) {
                System.out.print(operators.get(i)[j]+ " ");
            }
            System.out.println();
        }
    }

    private void printNumbers(int[] numbers) {
        for (int number: numbers) {
            System.out.print(number + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Beatrix24Solver solver = new Beatrix24Solver();
        int[] numbers = solver.generate4Numbers();
        System.out.print("4 random numbers: ");
        solver.printNumbers(numbers);
        if (!solver.solve(numbers)) {
            System.out.print("There is no solution!");
        }
    }

}

