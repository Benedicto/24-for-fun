package nijuyon.operator;

/**
 * Multiplication.
 */
public class Multiply implements Operator {
    public double operate(double a, double b) {
        return a * b;
    }

    public String toString() {
        return "*";
    }
}
