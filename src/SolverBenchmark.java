import nijuyon.operator.OperatorAggregate;

import java.time.Instant;
import java.util.List;

/**
 * Created by zhenhua.xu on 1/4/17.
 */
public class SolverBenchmark {

    public static void main(String[] args) {
        Beatrix24Solver beatrixSolver = new Beatrix24Solver();
        Ben24Solver benSolver = new Ben24Solver();
        Jonas24Solver jonasSolver = new Jonas24Solver();
        long minTime = Long.MAX_VALUE;
        for (int i=0; i<10; i++) {
            minTime = Math.min(minTime, benchMarkSolver(beatrixSolver));
        }
        System.out.println("\nThe calculation took " + minTime + " ms");
    }

    public static long benchMarkSolver(A24Solver solver) {
        long startTime = Instant.now().toEpochMilli();
        int totalCombinations = 0;
        int solvableCombinations = 0;
        int[] numbers = new int[4];
        for (int i=1; i<=9; i++) {
            for (int j=i; j<=9; j++) {
                for (int k=j; k<=9; k++) {
                    for (int l=k; l<=9; l++) {
                        numbers[0] = i;
                        numbers[1] = j;
                        numbers[2] = k;
                        numbers[3] = l;
                        totalCombinations++;
                        if(solver.solve(numbers)) {
                            solvableCombinations++;
                        }
                    }
                }
            }
        }

        long stopTime = Instant.now().toEpochMilli();
        long runningTime = stopTime - startTime;
        return runningTime;
    }

}
