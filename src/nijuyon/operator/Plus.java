package nijuyon.operator;

/**
 * Addition.
 */
public class Plus implements Operator {
    public double operate(double a, double b) {
        return a + b;
    }

    public String toString() {
        return "+";
    }
}
