/**
 * Created by zhenhua.xu on 1/4/17.
 */
public interface A24Solver {
    boolean solve(int[] numbers);
}
